var ganttData = [
	{
		id: 1, itemName: "Feature 1", series: [
			{ seriesName: "Task1", start: new Date(2010, 00, 01), end: new Date(2010, 00, 03) },
			{ seriesName: "Task2", start: new Date(2010, 00, 01), end: new Date(2010, 00, 03) }
		]
	}, 
	{
		id: 2, itemName: "Feature 2", series: [
			{ seriesName: "Planned", start: new Date(2010,00,05), end: new Date(2010,00,20) }
		]
	}, 
	{
		id: 3, itemName: "Feature 3", series: [
			{ seriesName: "Planned", start: new Date(2010,00,11), end: new Date(2010,01,03) }
		]
	}, 
	{
		id: 4, itemName: "Feature 4", series: [
			{ seriesName: "Planned", start: new Date(2010,01,01), end: new Date(2010,01,03) }
		]
	},
	{
		id: 5, itemName: "Feature 5", series: [
			{ seriesName: "Planned", start: new Date(2010,02,01), end: new Date(2010,03,20) }
		]
	}
];