﻿// Query String
function getQueryString(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function gotoUrl(url) {
    window.location = url;
}