function popupHighSlide(caption, url) {
    $('#highslide-html-selector').html('<iframe id="frame-highslide-popup" scrolling="no" src="" frameborder="0" width="100%" height="500"></iframe>');
    var frame = window.document.getElementById('frame-highslide-popup');

    frame.src = url; 
    
    alert(frame.src);

    // BEGIN: Set new id.
    var time = new Date();
    document.getElementById('frame-highslide-popup').id = window.document.getElementById('frame-highslide-popup').id + time;
    document.getElementById('highslide-html-selector').id = window.document.getElementById('highslide-html-selector').id + time;
    // END: Set new id.

    window.hs.htmlExpand(null, {
        contentId: 'highslide-html-selector' + time,
        width: 800,
        align: 'center',
        fadeInOut: false,
        dimmingOpacity: 0.6,
        headingText: caption
    });

    // restore id.
    windows.document.getElementById('frame-highslide-popup' + time).id = 'frame-highslide-popup';
    windows.document.getElementById('highslide-html-selector' + time).id = 'highslide-html-selector';
}


function PopupWindow(UrlPage, width, height) {
    var retVal = "";
    var win2;

    if (height == null) {
        height = "430";
    }

    if (width == null) {
        width = "900";
    }

    var iMyWidth = (window.screen.width / 2) - (width / 2); //half the screen width minus half the new window width (plus 5 pixel borders).
    var iMyHeight = (window.screen.height / 2) - (height / 2); //half the screen height minus half the new window height (plus title and status bars).


    if (win2 == null) {
        win2 = window.open(UrlPage, "aspnetPopup", "location=no, modal=yes, toolbar=no, menubar=no, status=no, height=" + height + ", width=" + width + ", resizable=yes, left=" + iMyWidth + ", top=" + iMyHeight + ", screenX=" + iMyWidth + ", screenY=" + iMyHeight + ", scrollbars=yes");
    }

    win2.focus();
}

function PopupWindowRefreshPage(UrlPage, width, height) {
    var retVal = "";
    var win2;

    if (height == null) {
        height = "430";
    }

    if (width == null) {
        width = "650";
    }

    var iMyWidth = (window.screen.width / 2) - (width / 2); //half the screen width minus half the new window width (plus 5 pixel borders).
    var iMyHeight = (window.screen.height / 2) - (height / 2); //half the screen height minus half the new window height (plus title and status bars).


    if (win2 == null) {
        win2 = window.open(UrlPage, "aspnetPopupRefreshPage", "location=no, modal=yes, toolbar=no, menubar=no, status=no, height=" + height + ", width=" + width + ", resizable=yes, left=" + iMyWidth + ", top=" + iMyHeight + ", screenX=" + iMyWidth + ", screenY=" + iMyHeight + ", scrollbars=yes");
    }

    win2.focus();
}